class CfgTasks {
    class MMMM {
        title = CSTRING(DisplayName); // Regular task title
        description = CSTRING(Mission_Description); // Regular description. Cannot use linebreaks (enters), if needed use stringtable.
        icon = "meet"; // Icon classname from https://community.bistudio.com/wiki/Arma_3_Tasks_Overhaul#Appendix
        onShowEvents[] = { "MMMMShown" };
        conditionEventsSuccess[] = {
            "BorderBreakthroughSuccessful",
            "PeninsulaConquered",
            "HQCaptured"
        };
        conditionEventsSuccessRequired = 3;
    };

/*
    Phase 1 - BorderBreakthrough tasks
*/
    class BorderBreakthrough {
        title = CSTRING(Task_BorderBreakthrough_Title);
        description = CSTRING(Task_BorderBreakthrough_Description);
        parentTask = "MMMM";
        icon = "danger";
        conditionEventsShow[] = { "MMMMShown" };
        conditionEventsSuccessRequired = 2;
        conditionEventsSuccess[] = {
            "IoanninaCaptured",
            "DelfinakiCaptured"
        };
        onShowEvents[] = { "BorderBreakthroughShown" };
        onSuccessEvents[] = { "BorderBreakthroughSuccessful" };
    };
    class Capture_Ioannina {
        title = CSTRING(Task_Capture_Ioannina_Title);
        description = CSTRING(Task_Capture_Ioannina_Description);
        parentTask = "BorderBreakthrough";
        marker = "marker_ioannina";
        icon = "attack";
        conditionEventsShow[] = { "BorderBreakthroughShown" };
        conditionCodeSuccess = "!(buildspawn_ioannina getVariable [""active"", true])";
        onSuccessEvents[] = { "IoanninaCaptured" };
    };
    class Capture_Delfinaki {
        title = CSTRING(Task_Capture_Delfinaki_Title);
        description = CSTRING(Task_Capture_Delfinaki_Description);
        parentTask = "BorderBreakthrough";
        marker = "marker_delfinaki";
        icon = "attack";
        conditionEventsShow[] = { "BorderBreakthroughShown" };
        conditionCodeSuccess = "!(buildspawn_delfinaki getVariable [""active"", true])";
        onSuccessEvents[] = { "DelfinakiCaptured" };
    };

/*
    Phase 2 - Conquer peninsula tasks
*/
    class Peninsula {
        title = CSTRING(Task_Peninsula_Title);
        description = CSTRING(Task_Peninsula_Description);
        parentTask = "MMMM";
        icon = "attack";
        conditionEventsShowRequired = 1;
        conditionEventsShow[] = {
            "IoanninaCaptured",
            "DelfinakiCaptured",
            "BorderBreakthroughSuccessful"
        };
        onShowEvents[] = { "PeninsulaDiscovered" };
        createdShowNotification = "true";
        conditionEventsSuccessRequired = 6;
        conditionEventsSuccess[] = {
            "FOBCaptured",
            "IntelRetrieved",
            "RadarDestroyed",
            "VIPCaptured",
            "VIPKilled",
            "SofiaCaptured",
            "MolosCaptured"
        };
        onSuccessEvents[] = { "PeninsulaConquered" };
    };
    class Capture_FOB {
        title = CSTRING(Task_Capture_FOB_Title);
        description = CSTRING(Task_Capture_FOB_Description);
        parentTask = "Peninsula";
        marker = "marker_sla_fob";
        icon = "attack";
        conditionEventsShow[] = { "PeninsulaDiscovered" };
        conditionCodeSuccess = "!(buildspawn_fob getVariable [""active"", true])";
        onSuccessEvents[] = { "FOBCaptured" };
    };
    class Intel {
        title = CSTRING(Task_Intel_Title);
        description = CSTRING(Task_Intel_Description);
        marker = "marker_forest";
        parentTask = "Peninsula";
        icon = "documents";
        conditionEventsShow[] = { "PeninsulaDiscovered" };
        conditionEventsSuccessRequired = 3;
        conditionEventsSuccess[] = {
            "LaptopFound",
            "TabletFound",
            "PhoneFound",
            "DocumentsFound",
            "PhotosFound"
        };
        onSuccessEvents[] = { "IntelRetrieved" };
    };
        class VIP {
        title = CSTRING(Task_VIP_Title);
        description = CSTRING(Task_VIP_Description);
        parentTask = "Peninsula";
        icon = "target";
        conditionEventsShow[] = { "PeninsulaDiscovered" };
        conditionCodeFailed = "!(alive vip)";
        conditionCodeSuccess = "vip inArea ""base_jail""";
        onSuccessEvents[] = { "VIPCaptured" };
        onFailedEvents[] = { "VIPKilled" };
    };
    class Destroy_Radar {
        title = CSTRING(Task_Destroy_Radar_Title);
        description = CSTRING(Task_Destroy_Radar_Description);
        marker = "marker_radar_base";
        parentTask = "Peninsula";
        icon = "destroy";
        conditionEventsShow[] = { "PeninsulaDiscovered" };
        conditionCodeSuccess = "!(alive sla_radar_big) && {!(alive sla_radar_small)}";
        onSuccessEvents[] = { "RadarDestroyed" };
    };
    class Capture_Sofia {
        title = CSTRING(Task_Capture_Sofia_Title);
        description = CSTRING(Task_Capture_Sofia_Description);
        parentTask = "Peninsula";
        marker = "marker_sofia";
        icon = "attack";
        conditionEventsShow[] = { "PeninsulaDiscovered" };
        conditionCodeSuccess = "!(buildspawn_sofia getVariable [""active"", true])";
        onSuccessEvents[] = { "SofiaCaptured" };
    };
    class Capture_Molos {
        title = CSTRING(Task_Capture_Molos_Title);
        description = CSTRING(Task_Capture_Molos_Description);
        marker = "marker_molos";
        parentTask = "Peninsula";
        icon = "attack";
        conditionEventsShow[] = { "PeninsulaDiscovered" };
        conditionCodeSuccess = "!(buildspawn_molos getVariable [""active"", true])";
        onSuccessEvents[] = { "MolosCaptured" };
    };

/*
    Phase 3 - EnemyHQ tasks
*/
    class EnemyHQ {
        title = CSTRING(Task_EnemyHQ_Title);
        description = CSTRING(Task_EnemyHQ_Description);
        marker = "marker_sla_hq";
        parentTask = "MMMM";
        icon = "attack";
        conditionEventsShow[] = { "PeninsulaConquered" };
        conditionEventsSuccess[] = { "HQCaptured" };
    };
};