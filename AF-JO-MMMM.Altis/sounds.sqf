v_arr_hurt = ["h1","h2","h3","h4","h5","h6","h7","h8"];
v_arr_fire = ["f1","f2","f3","f4","f5","f6","f7","f8","f9","f10","f11","t1","t2","t3","t4","t5","t6","t7","t8","t9","t10"];
v_arr_manDown = ["m1","m2","m3","m4","m5","m6","m7"];
v_arr_grenade = ["g1","g2","g3","g4","g5"];
v_arr_scare = ["s1","s2","s3","s4","s5","s6","s7"];
v_arr_agony = ["a1","a2","a3","a4","a5","a6","a7","a8","a9","a10","a11","a12"];
v_arr2_hurt = [] + v_arr_hurt;
v_arr2_fire = [] + v_arr_fire;
v_arr2_manDown = [] + v_arr_manDown;
v_arr2_grenade = [] + v_arr_grenade;
v_arr2_scare = [] + v_arr_scare;
v_arr2_agony = [] + v_arr_agony;
AF_fnc_voice_hurt = {
	_this select 0 setVariable ["M_AIvoice",false];
	if (v_arr2_hurt isEqualTo []) then {v_arr2_hurt = [] +  v_arr_hurt};
	private _r = floor random (count v_arr2_hurt);
	private _s = v_arr2_hurt select _r;
	[_this select 0,_s] remoteExec ["say3D", 0];
	[_this select 0] spawn
	{
		sleep (2 + random 5);
		(_this select 0) setVariable ["M_AIvoice",nil];
	};
	v_arr2_hurt deleteAt _r;
};
AF_fnc_voice_fire = {
	if (0.5 > random 1)exitWith {};
	if (v_arr2_fire isEqualTo []) then {v_arr2_fire = [] +  v_arr_fire};
	private _r = floor random (count v_arr2_fire);
	private _s = v_arr2_fire deleteAt _r;
	[_this select 0,_s] remoteExec ["say3D", 0];
	_s
};
AF_fnc_voice_manDown = {
	if (v_arr2_manDown isEqualTo []) then {v_arr2_manDown = [] + v_arr_manDown};
	private _r = floor random (count v_arr2_manDown);
	private _s = v_arr2_manDown deleteAt _r;
	[_this select 0,_s] remoteExec ["say3D", 0];
	_s
};
AF_fnc_voice_grenade = {
	if (v_arr2_grenade isEqualTo []) then {v_arr2_grenade = [] + v_arr_grenade};
	private _r = floor random (count v_arr2_grenade);
	private _s = v_arr2_grenade deleteAt _r;
	[_this select 0,_s] remoteExec ["say3D", 0];
	_s
};
AF_fnc_voice_scare = {
	if (v_arr2_scare isEqualTo []) then {v_arr2_scare = [] + v_arr_scare};
	private _r = floor random (count v_arr2_scare);
	private _s = v_arr2_scare deleteAt _r;
	[_this select 0,_s] remoteExec ["say3D", 0];
	_s
};
AF_fnc_voice_agony = {
	if (v_arr2_agony isEqualTo []) then {v_arr2_agony = [] + v_arr_agony};
	private _r = floor random (count v_arr2_agony);
	private _s = v_arr2_agony deleteAt _r;
	if (alive (_this select 0)) then
	{
		_this select 0 setVariable ["M_AIvoice",false];
		[_this select 0,_s] remoteExec ["say3D", 0];
		[_this select 0] spawn
		{
			sleep (2 + random 5);
			(_this select 0) setVariable ["M_AIvoice",nil];			
		};
	}else
	{
		[(_this select 0), _s] remoteExecCall ["AF_fnc_dieSound", 0, false];
	};
	_s
};

AF_fnc_dieSound = {
	params [["_body",objNull],"_s"];
	private _dummy = "#particlesource" createVehicleLocal ASLToAGL getPosWorld _body;
	_dummy say3D _s;
	[
		{
			params [["_dummy",objNull]];
			deleteVehicle _dummy;
		},
		[_dummy],
		3
	]call CBA_fnc_waitAndExecute;
};

if (isServer) then {
	["CAManBase", "fired", {
		params ["_unit", "_weapon", "_muzzle", "_mode", "_ammo", "_magazine", "_projectile", "_gunner"];
		if (isPlayer _unit)exitWith {};
		if (0.02 > random 1) then {
			[_unit] call AF_fnc_voice_fire;
		};
	}] call CBA_fnc_addClassEventHandler;

	addMissionEventHandler ["EntityKilled",
	{
		params ["_killed", "_killer", "_instigator"];
		if (isPlayer _killed)exitWith {
			[_killed] call AF_fnc_voice_agony;
		};
		if (_killed isKindOf "Man") then{
			if (0.75 > random 1) then {
				[_killed] call AF_fnc_voice_agony;
			};
			private _group = group _killed;
			private _randomUnit = selectRandom units _group;
			if (!alive _randomUnit)exitWith {};
			[
				{
					params [["_randomUnit",objNull]];
					if (!alive _randomUnit)exitWith {};
					if (0.5 > random 1) then {
						[_randomUnit] call AF_fnc_voice_manDown;
					}else{
						[_randomUnit] call AF_fnc_voice_scare;
					};
				},
				[_randomUnit],
				0.5 + random 1
			]call CBA_fnc_waitAndExecute;
		};
	}];
};

if (isServer || !hasInterface) then {
		["CAManBase", "Hit", {
		params ["_unit", "_source", "_damage", "_instigator"];
		if (isPlayer _unit)exitWith {};
		if !(local _unit)exitWith {};
		if (0.3 > random 1) then {
			private _lastScream = _unit getVariable ["lastScream", 0];
			if (_lastScream + 5 > time) exitWith {};
			_unit setVariable ["lastScream", time];
			[_unit] call AF_fnc_voice_hurt;
		};
	}] call CBA_fnc_addClassEventHandler;
};