#include "script_component.hpp"

// Disable CUP street lights based on lighting levels (bad performance script)
CUP_stopLampCheck = true;

[{alive player}, {
    // Dodanie akcji do zmiany odległości widzenia oraz nazw drużyn
    call FUNC(playerActions);
}, [], -1] call CBA_fnc_waitUntilAndExecute;

terminal addAction
[ 
    "Przebrajanie",
    { 
        params ["_target", "_caller", "_actionId", "_arguments"];
        [_target] call AF_fnc_casRearmOpen;
    },
    [],
    10,
    true,
    true,
    "",
    "true",
    1.5,
    false,
    "",
    ""
];

ammoContainer addAction
[ 
    "Przebrajanie",
    { 
        params ["_target", "_caller", "_actionId", "_arguments"];
        [_target] call AF_fnc_casRearmOpen;
    },
    [],
    10,
    true,
    true,
    "",
    "true",
    3,
    false,
    "",
    ""
];
