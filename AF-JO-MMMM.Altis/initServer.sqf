#include "script_component.hpp"

if (!isNil QUOTE(RESPAWN_HELPER_VR)) then {
    createMarker ["respawn", position RESPAWN_HELPER_VR];
};

AF_dispression = 5;
publicVariable "AF_dispression";

terminal setVariable ["distance", 30, true];
terminal setVariable ["position", rearmPos, true];

ammoContainer setVariable ["rearmVehType","LandVehicle", true];
