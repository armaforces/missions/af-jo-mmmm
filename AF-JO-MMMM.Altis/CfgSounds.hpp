class CfgSounds
{
	sounds[] = {};
	class h1
	{
		name = "h1";
		sound[] = { "sounds\h1.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class h2
	{
		name = "h2";
		sound[] = { "sounds\h2.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class h3
	{
		name = "h3";
		sound[] = { "sounds\h3.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class h4
	{
		name = "h4";
		sound[] = { "sounds\h4.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class h5
	{
		name = "h5";
		sound[] = { "sounds\h5.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class h6
	{
		name = "h6";
		sound[] = { "sounds\h6.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class h7
	{
		name = "h7";
		sound[] = { "sounds\h7.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class h8
	{
		name = "h8";
		sound[] = { "sounds\h8.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class h9
	{
		name = "h9";
		sound[] = { "sounds\h9.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class m1
	{
		name = "m1";
		sound[] = { "sounds\m1.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class m2
	{
		name = "m2";
		sound[] = { "sounds\m2.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class m3
	{
		name = "m3";
		sound[] = { "sounds\m3.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class m4
	{
		name = "m4";
		sound[] = { "sounds\m4.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class m5
	{
		name = "m5";
		sound[] = { "sounds\m5.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class m6
	{
		name = "m6";
		sound[] = { "sounds\m6.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class m7
	{
		name = "m7";
		sound[] = { "sounds\m7.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class g1
	{
		name = "g1";
		sound[] = { "sounds\g1.ogg", 6, 1, 150 };
		titles[] = { 0, "" };
	};
	class g2
	{
		name = "g2";
		sound[] = { "sounds\g2.ogg", 6, 1, 150 };
		titles[] = { 0, "" };
	};
	class g3
	{
		name = "g3";
		sound[] = { "sounds\g3.ogg", 6, 1, 150 };
		titles[] = { 0, "" };
	};
	class g4
	{
		name = "g4";
		sound[] = { "sounds\g4.ogg", 6, 1, 150 };
		titles[] = { 0, "" };
	};
	class g5
	{
		name = "g5";
		sound[] = { "sounds\g5.ogg", 6, 1, 150 };
		titles[] = { 0, "" };
	};
	class s1
	{
		name = "s1";
		sound[] = { "sounds\s1.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class s2
	{
		name = "s2";
		sound[] = { "sounds\s2.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class s3
	{
		name = "s3";
		sound[] = { "sounds\s3.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class s4
	{
		name = "s4";
		sound[] = { "sounds\s4.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class s5
	{
		name = "s5";
		sound[] = { "sounds\s5.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class s6
	{
		name = "s6";
		sound[] = { "sounds\s6.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class s7
	{
		name = "s7";
		sound[] = { "sounds\s7.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class s8
	{
		name = "s8";
		sound[] = { "sounds\s8.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class s9
	{
		name = "s9";
		sound[] = { "sounds\s9.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class t1
	{
		name = "t1";
		sound[] = { "sounds\t1.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class t2
	{
		name = "t2";
		sound[] = { "sounds\t2.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class t3
	{
		name = "t3";
		sound[] = { "sounds\t3.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class t4
	{
		name = "t4";
		sound[] = { "sounds\t4.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class t5
	{
		name = "t5";
		sound[] = { "sounds\t5.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class t6
	{
		name = "t6";
		sound[] = { "sounds\t6.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class t7
	{
		name = "t7";
		sound[] = { "sounds\t7.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class t8
	{
		name = "t8";
		sound[] = { "sounds\t8.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class t9
	{
		name = "t9";
		sound[] = { "sounds\t9.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class t10
	{
		name = "t10";
		sound[] = { "sounds\t10.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class f1
	{
		name = "f1";
		sound[] = { "sounds\f1.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class f2
	{
		name = "f2";
		sound[] = { "sounds\f2.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class f3
	{
		name = "f3";
		sound[] = { "sounds\f3.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class f4
	{
		name = "f4";
		sound[] = { "sounds\f4.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class f5
	{
		name = "f5";
		sound[] = { "sounds\f5.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class f6
	{
		name = "f6";
		sound[] = { "sounds\f6.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class f7
	{
		name = "f7";
		sound[] = { "sounds\f7.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class f8
	{
		name = "f8";
		sound[] = { "sounds\f8.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class f9
	{
		name = "f9";
		sound[] = { "sounds\f9.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class f10
	{
		name = "f10";
		sound[] = { "sounds\f10.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class f11
	{
		name = "f11";
		sound[] = { "sounds\f11.ogg", 4, 1, 100 };
		titles[] = { 0, "" };
	};
	class a1
	{
		name = "a1";
		sound[] = { "sounds\a1.ogg", 3, 1, 100 };
		titles[] = { 0, "" };
	};
	class a2
	{
		name = "a2";
		sound[] = { "sounds\a2.ogg", 3, 1, 100 };
		titles[] = { 0, "" };
	};
	class a3
	{
		name = "a3";
		sound[] = { "sounds\a3.ogg", 3, 1, 100 };
		titles[] = { 0, "" };
	};
	class a4
	{
		name = "a4";
		sound[] = { "sounds\a4.ogg", 3, 1, 100 };
		titles[] = { 0, "" };
	};
	class a5
	{
		name = "a5";
		sound[] = { "sounds\a5.ogg", 3, 1, 100 };
		titles[] = { 0, "" };
	};
	class a6
	{
		name = "a6";
		sound[] = { "sounds\a6.ogg", 3, 1, 100 };
		titles[] = { 0, "" };
	};
	class a7
	{
		name = "a7";
		sound[] = { "sounds\a7.ogg", 3, 1, 100 };
		titles[] = { 0, "" };
	};
	class a8
	{
		name = "a8";
		sound[] = { "sounds\a8.ogg", 3, 1, 100 };
		titles[] = { 0, "" };
	};
	class a9
	{
		name = "a9";
		sound[] = { "sounds\a9.ogg", 3, 1, 100 };
		titles[] = { 0, "" };
	};
	class a10
	{
		name = "a10";
		sound[] = { "sounds\a10.ogg", 3, 1, 100 };
		titles[] = { 0, "" };
	};
	class a11
	{
		name = "a11";
		sound[] = { "sounds\a11.ogg", 3, 1, 100 };
		titles[] = { 0, "" };
	};
	class a12
	{
		name = "a12";
		sound[] = { "sounds\a12.ogg", 3, 1, 100 };
		titles[] = { 0, "" };
	};
};