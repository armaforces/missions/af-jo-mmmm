/* #Lumyhy
$[
	1.063,
	["AF_rearm",[[0,0,1,1],0.025,0.04,"GUI_GRID"],0,0,0],
	[2200,"",[1,"",["0.29375 * safezoneW + safezoneX","0.225 * safezoneH + safezoneY","0.4125 * safezoneW","0.55 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[1502,"",[1,"",["0.304062 * safezoneW + safezoneX","0.335 * safezoneH + safezoneY","0.12375 * safezoneW","0.33 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[1500,"",[1,"",["0.438125 * safezoneW + safezoneX","0.335 * safezoneH + safezoneY","0.12375 * safezoneW","0.33 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[1501,"",[1,"",["0.572187 * safezoneW + safezoneX","0.335 * safezoneH + safezoneY","0.12375 * safezoneW","0.33 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[1000,"",[1,"Przezbrajanie",["0.304062 * safezoneW + safezoneX","0.247 * safezoneH + safezoneY","0.195937 * safezoneW","0.066 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","2"],[]],
	[1600,"",[1,"Zamknij",["0.304062 * safezoneW + safezoneX","0.687 * safezoneH + safezoneY","0.12375 * safezoneW","0.066 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]],
	[1601,"",[1,"Przezbrajanie",["0.572187 * safezoneW + safezoneX","0.687 * safezoneH + safezoneY","0.12375 * safezoneW","0.066 * safezoneH"],[-1,-1,-1,-1],[-1,-1,-1,-1],[-1,-1,-1,-1],"","-1"],[]]
]
*/
class AF_dlg_rearm {
	idd = 20;
	movingenable = false;
	////////////////////////////////////////////////////////
	// GUI EDITOR OUTPUT START (by Madin, v1.063, #Lumyhy)
	////////////////////////////////////////////////////////
	class controls {
		class IGUIBack_2200: IGUIBack
		{
			idc = 2200;
			x = 0.262812 * safezoneW + safezoneX;
			y = 0.225 * safezoneH + safezoneY;
			w = 0.474375 * safezoneW;
			h = 0.55 * safezoneH;
		};
		class RscListbox_1501: RscListbox
		{
			idc = 1501;
            x = 0.273125 * safezoneW + safezoneX;
            y = 0.335 * safezoneH + safezoneY;
            w = 0.144375 * safezoneW;
            h = 0.33 * safezoneH;

			type = 102;
			drawSideArrows = 0;
			shadow = 1;
			idcLeft = -1;
			idcRight = -1;
			columns[] = {0,0.5};
			colorPicture[] = {1,1,1,1};
			colorPictureSelected[] = {1,1,1,1};
			colorPictureDisabled[] = {1,1,1,1};
			sizeEx = 0.0375;
		};
		class RscListbox_1502: RscListbox
		{
			idc = 1502;
            x = 0.427812 * safezoneW + safezoneX;
            y = 0.335 * safezoneH + safezoneY;
            w = 0.144375 * safezoneW;
            h = 0.33 * safezoneH;

			onLBSelChanged = "[missionNameSpace getVariable ['AF_casRearmController', objNull]] call AF_fnc_casRearmSlotSelect";

			type = 102;
			drawSideArrows = 0;
			shadow = 1;
			idcLeft = -1;
			idcRight = -1;
			columns[] = {0,0.08,0.65};
			colorPicture[] = {1,1,1,1};
			colorPictureSelected[] = {1,1,1,1};
			colorPictureDisabled[] = {1,1,1,1};
			sizeEx = 0.0375;
		};
		class RscListbox_1503: RscListbox
		{
			idc = 1503;
            x = 0.5825 * safezoneW + safezoneX;
            y = 0.335 * safezoneH + safezoneY;
            w = 0.144375 * safezoneW;
            h = 0.33 * safezoneH;

			type = 102;
			drawSideArrows = 0;
			shadow = 1;
			idcLeft = -1;
			idcRight = -1;
			columns[] = {0,0.675,0.825};
			colorPicture[] = {1,1,1,1};
			colorPictureSelected[] = {1,1,1,1};
			colorPictureDisabled[] = {1,1,1,1};
			sizeEx = 0.0375;
		};
		class RscText_1000: RscText
		{
			idc = 1000;
			text = "Przezbrajanie"; //--- ToDo: Localize;
            x = 0.273125 * safezoneW + safezoneX;
            y = 0.247 * safezoneH + safezoneY;
            w = 0.195937 * safezoneW;
            h = 0.066 * safezoneH;
			sizeEx = 2 * GUI_GRID_H;
		};
		class RscButton_1600: RscButton
		{
			idc = 1600;

			text = "Zamknij"; //--- ToDo: Localize;
            x = 0.273125 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.066 * safezoneH;
			action = "closeDialog 0";
		};
		class RscButton_1601: RscButton
		{
			idc = 1601;

			text = "Zdejmij wszystkie"; //--- ToDo: Localize;
			x = 0.427812 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.066 * safezoneH;

			action = "[missionNameSpace getVariable ['AF_casRearmController', objNull], lnbValue [1502, [lbCurSel 1502, 0]], lnbData [1502, [lbCurSel 1502, 0]]] remoteExecCall ['AF_fnc_casRearmRemoveMags', 2, false]";
		};
		/*
		class RscButton_1602: RscButton
		{
			idc = 1602;

			text = "Zdejmij 1"; //--- ToDo: Localize;
			x = 0.505156 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.066 * safezoneH;
		};
		*/
		class RscButton_1603: RscButton
		{
			idc = 1603;

			text = "Załóż 1"; //--- ToDo: Localize;
			x = 0.5825 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.066 * safezoneH;

			action = "[missionNameSpace getVariable ['AF_casRearmController', objNull], lnbValue [1502, [lbCurSel 1502, 0]], lnbData [1502, [lbCurSel 1502, 0]], lnbValue [1503, [lbCurSel 1503, 0]], false] remoteExecCall ['AF_fnc_casRearmAddMag', 2, false]";
		};
		class RscButton_1604: RscButton
		{
			idc = 1604;

			text = "Załóż wszystkie"; //--- ToDo: Localize;
			x = 0.659844 * safezoneW + safezoneX;
			y = 0.687 * safezoneH + safezoneY;
			w = 0.0670312 * safezoneW;
			h = 0.066 * safezoneH;

			action = "[missionNameSpace getVariable ['AF_casRearmController', objNull], lnbValue [1502, [lbCurSel 1502, 0]], lnbData [1502, [lbCurSel 1502, 0]], lnbValue [1503, [lbCurSel 1503, 0]], true] remoteExecCall ['AF_fnc_casRearmAddMag', 2, false]";
		};
		////////////////////////////////////////////////////////
		// GUI EDITOR OUTPUT END
		////////////////////////////////////////////////////////
	};
};