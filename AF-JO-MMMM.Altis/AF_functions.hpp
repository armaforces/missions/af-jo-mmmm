class ADDON {
    tag = QUOTE(ADDON);
    class functions {
        file = "functions";
        
        // For ViewDistance and GroupRename GUIs
        class groupRenameGUI {};
        class groupRenameList {};
        class setViewDistance {};
        class viewDistanceGUI {};
        class playerActions {};

        // Mission specific
        class intelActions { postInit = 1; };
    };
};

class AF {
    /*
        custom

        jednorazowe funkcje
    */
    class custom {
        class addDispression {};
        class damageModifier {};
    };
};