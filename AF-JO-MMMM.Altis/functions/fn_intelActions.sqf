#include "script_component.hpp"
/*
 * Author: 3Mydlo3
 * Hold actions for intel retrieval.
 *
 * Arguments:
 * None
 *
 * Return Value:
 * None
 *
 * Public: No
 */

if (!hasInterface) exitWith { nil };

[laptopIntel, LLSTRING(Gather_Intel), "", "", "true", "true", {}, {}, {
    ["LaptopFound"] call CBA_fnc_serverEvent;
	deleteVehicle (_this select 0);
}, {}, [], 2] call BIS_fnc_holdActionAdd;

[tabletIntel, LLSTRING(Gather_Intel), "", "", "true", "true", {}, {}, {
    ["TabletFound"] call CBA_fnc_serverEvent;
	deleteVehicle (_this select 0);
}, {}, [], 2] call BIS_fnc_holdActionAdd;

[phoneIntel, LLSTRING(Gather_Intel), "", "", "true", "true", {}, {}, {
    ["PhoneFound"] call CBA_fnc_serverEvent;
	deleteVehicle (_this select 0);
}, {}, [], 2] call BIS_fnc_holdActionAdd;

[documentsIntel, LLSTRING(Gather_Intel), "", "", "true", "true", {}, {}, {
    ["DocumentsFound"] call CBA_fnc_serverEvent;
	deleteVehicle (_this select 0);
}, {}, [], 2] call BIS_fnc_holdActionAdd;

[photosIntel, LLSTRING(Gather_Intel), "", "", "true", "true", {}, {}, {
    ["PhotosFound"] call CBA_fnc_serverEvent;
	deleteVehicle (_this select 0);
}, {}, [], 2] call BIS_fnc_holdActionAdd;
