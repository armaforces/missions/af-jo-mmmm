[
	// Global var name
	"defaultviewdistance",
	// Setting type
	"SLIDER",
	// Title, Tooltip
	[
		"Domyślna odległość widzenia",
		"odległość widzenia na start misji."
	],
	// Category, Subcategory
	["_Ustawienia misji", "Odległość widzenia"],
	// Extra params, depending on settings type
	[0, 12000, 1500, 0],
	// Is global
	true,
	// Init/Change script
	{},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

[
	// Global var name
	"maxviewdistance",
	// Setting type
	"SLIDER",
	// Title, Tooltip
	[
		"Maksymalna odległość widzenia",
		"Maksymalna odległość widzenia, którą można ustawić na misji."
	],
	// Category, Subcategory
	["_Ustawienia misji", "Odległość widzenia"],
	// Extra params, depending on settings type
	[0, 12000, 10000, 0],
	// Is global
	true,
	// Init/Change script
	{},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

[
	// Global var name
	"minviewdistance",
	// Setting type
	"SLIDER",
	// Title, Tooltip
	[
		"Minimalna odległość widzenia",
		"Minimalna odległość widzenia, którą można ustawić na misji."
	],
	// Category, Subcategory
	["_Ustawienia misji", "Odległość widzenia"],
	// Extra params, depending on settings type
	[0, 12000, 500, 0],
	// Is global
	true,
	// Init/Change script
	{},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

[
	// Global var name
	"AF_DetailDistance",
	// Setting type
	"SLIDER",
	// Title, Tooltip
	[
		"Trawa i detale terenu",
		"Ustawianie detali trawy i terenu, uniemożliwia wyłączenie trawy na klientach. 0 maksymalne detale, 50 brak trawy. -1 by wyłączyć wymuszanie detali."
	],
	// Category, Subcategory
	["_Ustawienia misji", "Odległość widzenia"],
	// Extra params, depending on settings type
	[-1, 50, 10, 0],
	// Is global
	true,
	// Init/Change script
	{},
	// Needs mission restart
	true
] call CBA_Settings_fnc_init;