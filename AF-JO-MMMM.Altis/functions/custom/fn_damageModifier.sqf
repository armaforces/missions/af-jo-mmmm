 /*
	AF_fnc_damageModifier

	Description:
		Add additional HP to unit.

	Arguments:
		0: Unit					<OBJECT>
		0: HP modifier value	<NUMBER>

	Return Value:
		None

*/

params [["_unit",objNull],["_HPmodifier",0.75]];
_unit setVariable ["HPmodifier", 0.75];
_unit addEventHandler ["HandleDamage", { 
	private _unit = _this select 0; 
	private _selection = _this select 1; 
	private _damage = _this select 2; 

	if (_selection == "?") exitWith {}; 

	_curDamage = damage _unit; 
	if (_selection != "") then {_curDamage = _unit getHit _selection}; 
	_newDamage = _damage - _curDamage; 

	_damage - _newDamage * (_unit getVariable ["HPmodifier", 0.75])
}];