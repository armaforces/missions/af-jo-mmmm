 /*
	AF_fnc_addDispressionWeapon

	Description:
		Add additional disperssion to specific guns.

	Arguments:
		0: Unit		<OBJECT>

	Return Value:
		None

*/

params [["_unit",objNull], ["_weapons", []]];
private _weaponsForDispression = _unit getVariable ["AF_weaponsForDispression", []];
{
	_weaponsForDispression pushBack _x;
}forEach _weapons;
_unit setVariable ["AF_weaponsForDispression", _weaponsForDispression];

_unit addEventHandler ["Fired", 
{
	params ["_unit", "_weapon", "_muzzle", "_mode", "_ammo", "_magazine", "_projectile", "_gunner"];
	if !(local _gunner) exitWith {};
	private _weaponsForDispression = _unit getVariable ["AF_weaponsForDispression", []];
	if !(_weapon in _weaponsForDispression) exitWith {};
	private _vel = (velocityModelSpace _projectile);
	private _force = random (_unit getVariable ["AF_dispression", 0.05]) * (_vel select 1); 
	private _angle = random 360;
	_projectile setVelocityModelSpace (_vel vectorAdd [_force * sin _angle, 0, _force * cos _angle]);
}];